# A Common Tracking Software (ACTS) Project - Data Repository {#mainpage}
This repository contains data needed to run examples in Acts.

1. [MagneticField](#bfield)

# <a name="bfield">MagneticField</a>
This directory contains magnetic field maps of ATLAS and FCChh in txt/csv 
and root format.